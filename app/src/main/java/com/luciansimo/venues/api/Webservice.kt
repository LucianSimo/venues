package com.luciansimo.venues.api

import com.luciansimo.venues.model.SearchResponse
import com.luciansimo.venues.model.VenueDetails

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Webservice {
    @GET("search?")
    fun getVenues(@Query("ll") ll: String,
                  @Query("limit") limit: Int,
                  @Query("radius") radius: Int,
                  @Query("client_id") clientId: String,
                  @Query("client_secret") clientSecret: String,
                  @Query("v") version: String): Call<SearchResponse>

    @GET("{venueId}")
    fun getVenueDetails(@Path("venueId") venueId: String): Call<VenueDetails>
}