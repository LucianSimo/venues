package com.luciansimo.venues.model

import com.google.gson.annotations.SerializedName

class SearchResult (
    @field:SerializedName("venues") var venues: List<Venue>
)