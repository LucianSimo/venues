package com.luciansimo.venues.model

import com.google.gson.annotations.SerializedName

class VenueLocation (
    @field:SerializedName("address") var address: String,
    @field:SerializedName("postalCode") var postalCode: String,
    @field:SerializedName("city") var city: String
)