package com.luciansimo.venues.model

import com.google.gson.annotations.SerializedName

class VenueDetails (
    @field:SerializedName("id") var id: String,
    @field:SerializedName("name") var name: String,
    @field:SerializedName("description") var description: String,
    @field:SerializedName("rating") var rating: Double,
    @field:SerializedName("ratingSignals") var ratingSignals: Int,
    //@field:SerializedName("photos") var photos: VenuePhotos,
    @field:SerializedName("location") var location: VenueLocation,
    @field:SerializedName("contact") var contact: VenueContact
)