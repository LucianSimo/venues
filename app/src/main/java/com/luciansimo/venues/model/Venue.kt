package com.luciansimo.venues.model

import com.google.gson.annotations.SerializedName

class Venue (
    @field:SerializedName("id") var id: String,
    @field:SerializedName("name") var name: String,
    @field:SerializedName("location") var location: VenueLocation
)