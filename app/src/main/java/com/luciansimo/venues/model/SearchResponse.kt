package com.luciansimo.venues.model

import com.google.gson.annotations.SerializedName

class SearchResponse (
    @field:SerializedName("response") var response: SearchResult
)