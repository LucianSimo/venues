package com.luciansimo.venues.model

import com.google.gson.annotations.SerializedName

class VenueContact (
    @field:SerializedName("formattedPhone") var formattedPhone: String,
    @field:SerializedName("twitter") var twitter: String,
    @field:SerializedName("instagram") var instagram: String
)