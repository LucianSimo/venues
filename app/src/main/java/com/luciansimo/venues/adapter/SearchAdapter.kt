package com.luciansimo.venues.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.luciansimo.venues.R
import com.luciansimo.venues.databinding.LayoutVenueBinding
import com.luciansimo.venues.model.Venue

class SearchAdapter(
    private val callback: (String) -> Unit
) : RecyclerView.Adapter<SearchAdapter.VenuesHolder>() {

    private lateinit var context: Context
    private var data: List<Venue> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenuesHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: LayoutVenueBinding = LayoutVenueBinding.inflate(inflater, parent, false)
        context = parent.context

        return VenuesHolder(binding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: VenuesHolder, position: Int) {
        data[position].let { venue ->
            holder.bind(venue)
            holder.itemView.setOnClickListener { callback(venue.id)}
        }
    }

    @Suppress("SENSELESS_COMPARISON")
    inner class VenuesHolder(private val binding: LayoutVenueBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(venue: Venue) {
            val city: String = if (venue.location.city == null || venue.location.city.isEmpty()) "" else venue.location.city
            val address: String = if (venue.location.address == null || venue.location.address.isEmpty()) "" else venue.location.address + ", "
            val postCode: String = if (venue.location.postalCode == null || venue.location.postalCode.isEmpty()) "" else venue.location.postalCode + ", "
            val location: String = if (city.isEmpty() && address.isEmpty() && postCode.isEmpty())
                context.getString(R.string.unknown_location) else "$address$postCode$city"

            binding.title.text = venue.name
            binding.location.text = location
        }
    }

    fun setData(venues: List<Venue>) {
        data = venues
        notifyDataSetChanged()
    }
}