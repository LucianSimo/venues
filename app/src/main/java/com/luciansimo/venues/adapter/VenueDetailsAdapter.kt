package com.luciansimo.venues.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.luciansimo.venues.databinding.*
import com.luciansimo.venues.model.VenueDetails

class VenueDetailsAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private enum class ItemType {
        IMAGE,
        INFO,
        RATING,
        CONTACT
    }

    private var venue: VenueDetails? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)

        return when (getItemViewType(viewType)) {
            ItemType.IMAGE.ordinal -> {
                ImageHolder(LayoutVenueImageBinding.inflate(layoutInflater, parent, false))
            }
            ItemType.INFO.ordinal -> {
                InfoHolder(LayoutVenueInfoBinding.inflate(layoutInflater, parent, false))
            }
            ItemType.RATING.ordinal -> {
                RatingHolder(LayoutVenueRatingBinding.inflate(layoutInflater, parent, false))
            }
            ItemType.CONTACT.ordinal -> {
                ContactHolder(LayoutVenueContactInfoBinding.inflate(layoutInflater, parent, false))
            }
            else -> throw IllegalArgumentException("Invalid view type: $viewType")
        }
    }

    override fun getItemCount(): Int {
        return if (venue != null) 4 else 0
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> ItemType.IMAGE.ordinal
            1 -> ItemType.INFO.ordinal
            2 -> ItemType.RATING.ordinal
            3 -> ItemType.CONTACT.ordinal
            else -> throw IllegalArgumentException("Invalid position: $position")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            ItemType.IMAGE.ordinal -> {
                (holder as ImageHolder).bind()
            }
            ItemType.INFO.ordinal -> {
                (holder as InfoHolder).bind()
            }
            ItemType.RATING.ordinal -> {
                (holder as RatingHolder).bind()
            }
            ItemType.CONTACT.ordinal -> {
                (holder as ContactHolder).bind()
            }
        }
    }

    private class ImageHolder(private val binding: LayoutVenueImageBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind() {}
    }

    private class InfoHolder(private val binding: LayoutVenueInfoBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind() {}
    }

    private class RatingHolder(private val binding: LayoutVenueRatingBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind() {}
    }

    private class ContactHolder(private val binding: LayoutVenueContactInfoBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind() {}
    }

    fun setVenue(venue: VenueDetails) {
        this.venue = venue
        notifyDataSetChanged()
    }
}