package com.luciansimo.venues.database

import com.luciansimo.venues.model.Venue
import com.luciansimo.venues.model.VenueContact
import com.luciansimo.venues.model.VenueDetails
import com.luciansimo.venues.model.VenueLocation

class VenueRepository(private val venueDao: VenueDao, private val venueDetailsDao: VenueDetailsDao) {
    fun getCoordinate(query: String): String {
        return venueDao.getCoordinate(query)
    }

    fun getVenueDetails(venueId: String): VenueDetails {
        return convertToVenueDetails(venueDetailsDao.getVenue(venueId))
    }

    fun getVenues(coordinates: String): List<Venue> {
        return convertFromVenueData(venueDao.getVenues(coordinates))
    }

    suspend fun insert(venues: List<Venue>, query: String, coordinates: String) {
        val venueData: List<VenueData> = convertToVenueData(venues, query, coordinates)
        venueData.forEach { venue ->
            venueDao.insertVenue(venue)
        }
    }

    suspend fun insertDetails(venue: VenueDetails) {
        venueDetailsDao.insertVenueDetails(convertFromVenueDetailsData(venue))
    }

    private fun convertFromVenueDetailsData(venue: VenueDetails): VenueDetailsData {
        return VenueDetailsData(venue.id, venue.name, venue.location.address,
            venue.location.postalCode, venue.location.city, venue.contact.formattedPhone,
            venue.contact.instagram, venue.rating, venue.ratingSignals, venue.description,
            venue.contact.twitter)
    }

    private fun convertToVenueDetails(venueData: VenueDetailsData): VenueDetails {
        return VenueDetails(venueData.id, venueData.name, venueData.description, venueData.rating,
            venueData.ratingSignals, VenueLocation(venueData.address, venueData.postalCode, venueData.city),
            VenueContact(venueData.phone, venueData.twitter, venueData.instagram))
    }

    private fun convertFromVenueData(venueData: List<VenueData>): List<Venue> {
        val venueList: MutableList<Venue> = mutableListOf()
        venueData.forEach { venue ->
            venueList.add(Venue(venue.id, venue.name, VenueLocation(venue.address, venue.postalCode, venue.city)))
        }

        return venueList
    }

    @Suppress("SENSELESS_COMPARISON")
    private fun convertToVenueData(venues: List<Venue>, query: String, coordinates: String): List<VenueData> { // TODO: write unit tests
        val venueDataList: MutableList<VenueData> = mutableListOf()
        venues.forEach { venue ->
            val postalCode: String = if (venue.location.postalCode == null) "" else venue.location.postalCode
            val address: String = if (venue.location.address == null) "" else venue.location.address
            val city: String = if (venue.location.city == null) "" else venue.location.city
            venueDataList.add(VenueData(venue.id, venue.name, address, postalCode, city, query, coordinates))
        }

        return venueDataList
    }
}

