package com.luciansimo.venues.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface VenueDetailsDao {
    @Query("SELECT * FROM venue_details_table WHERE id = :venueId")
    fun getVenue(venueId: String): VenueDetailsData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVenueDetails(venue: VenueDetailsData)
}