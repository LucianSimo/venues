package com.luciansimo.venues.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "venue_details_table")
data class VenueDetailsData(@PrimaryKey @ColumnInfo(name = "id") val id: String,
                            @ColumnInfo(name = "name") val name: String,
                            @ColumnInfo(name = "address") val address: String,
                            @ColumnInfo(name = "postalCode") val postalCode: String,
                            @ColumnInfo(name = "city") val city: String,
                            @ColumnInfo(name = "formattedPhone") val phone: String,
                            @ColumnInfo(name = "instagram") val instagram: String,
                            @ColumnInfo(name = "rating") val rating: Double,
                            @ColumnInfo(name = "ratingSignals") val ratingSignals: Int,
                            @ColumnInfo(name = "description") val description: String,
                            @ColumnInfo(name = "twitter") val twitter: String)