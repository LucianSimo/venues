package com.luciansimo.venues.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [VenueData::class, VenueDetailsData::class], version = 1, exportSchema = false)
abstract class VenueRoomDatabase: RoomDatabase() {

    abstract fun venueDao(): VenueDao
    abstract fun venueDetailsDao(): VenueDetailsDao

    companion object {
        @Volatile
        private var INSTANCE: VenueRoomDatabase? = null

        fun getDatabase(context: Context): VenueRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext, VenueRoomDatabase::class.java, "venue_database").build()
                INSTANCE = instance
                return instance
            }
        }
    }
}