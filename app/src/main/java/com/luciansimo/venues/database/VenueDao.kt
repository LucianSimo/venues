package com.luciansimo.venues.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface VenueDao {
    @Query("SELECT coordinates FROM venue_table WHERE searchQuery = :query LIMIT 1")
    fun getCoordinate(query: String): String

    @Query("SELECT * FROM venue_table WHERE coordinates = :coordinates")
    fun getVenues(coordinates: String): List<VenueData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVenue(venue: VenueData)
}