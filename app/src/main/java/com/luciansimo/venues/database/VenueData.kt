package com.luciansimo.venues.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "venue_table")
data class VenueData(@PrimaryKey @ColumnInfo(name = "id") val id: String,
                @ColumnInfo(name = "name") val name: String,
                @ColumnInfo(name = "address") val address: String,
                @ColumnInfo(name = "postalCode") val postalCode: String,
                @ColumnInfo(name = "city") val city: String,
                @ColumnInfo(name = "searchQuery") val searchQuery: String,
                @ColumnInfo(name = "coordinates") val coordinates: String)