package com.luciansimo.venues.util

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


object Util {
    fun isOnline(context: Context): Boolean {
        val connectivityManager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network: Network? = connectivityManager.activeNetwork
        if (network != null) {
            val networkCapabilities: NetworkCapabilities? = connectivityManager.getNetworkCapabilities(network)
            return networkCapabilities!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || networkCapabilities.hasTransport(
                NetworkCapabilities.TRANSPORT_WIFI)
        }

        return false
    }

    fun getCurrentDate(): String {
        val formatter = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
        return formatter.format(Calendar.getInstance().time)
    }

    fun getLocation(context: Context, query: String): String? {
        var addressList: List<Address>? = null

        val geoCoder = Geocoder(context)
        try {
            addressList = geoCoder.getFromLocationName(query, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return if (addressList == null || addressList.isEmpty()) {
            null
        } else {
            val address: Address = addressList.first()
            val lat: Double = address.latitude
            val lng: Double = address.longitude

            "$lat,$lng"
        }
    }
}