package com.luciansimo.venues.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope

import com.luciansimo.venues.api.RetrofitClientInstance.retrofitInstance
import com.luciansimo.venues.api.Webservice
import com.luciansimo.venues.database.*
import com.luciansimo.venues.model.SearchResponse
import com.luciansimo.venues.model.Venue
import com.luciansimo.venues.model.VenueDetails
import com.luciansimo.venues.util.Util

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class VenueViewModel(application: Application) : AndroidViewModel(application) {
    private val clientId = "PKDRQBFAMULO5BF1BQBBAIZIUNMUUZTU4F3UPMH5GXAMX4ZK" // TODO: safely store the keys using Android keystore API
    private val clientSecret = "VFK3TCSAID3RFJHSDFR1PM11O3BMWX0ENFQNR5MW0FQNLWXM"

    val searchResult: MutableLiveData<List<Venue>> by lazy {
        MutableLiveData<List<Venue>>()
    }

    val coordinates: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val venue: MutableLiveData<VenueDetails> by lazy {
        MutableLiveData<VenueDetails>()
    }

    private val venueDao: VenueDao = VenueRoomDatabase.getDatabase(application).venueDao()
    private val venueDetailsDao: VenueDetailsDao = VenueRoomDatabase.getDatabase(application).venueDetailsDao()
    private val repository: VenueRepository = VenueRepository(venueDao, venueDetailsDao)

    fun insert(venues: List<Venue>, query: String, coordinates: String) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(venues, query, coordinates)
    }

    fun insertDetails(venueDetails: VenueDetails) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertDetails(venueDetails)
    }

    private fun getVenue(coordinates: String) = viewModelScope.launch(Dispatchers.IO) {
        searchResult.postValue(repository.getVenues(coordinates))
    }

    fun getCoordinate(query: String) = viewModelScope.launch(Dispatchers.IO) {
        coordinates.postValue(repository.getCoordinate(query))
    }

    private fun getVenueDetailsFromDB(venueId: String) = viewModelScope.launch(Dispatchers.IO) {
        venue.postValue(repository.getVenueDetails(venueId))
    }

    fun searchVenues(query: String, coordinates: String, limit: Int, radius: Int) {
        if (Util.isOnline(getApplication())) {
            val service: Webservice = retrofitInstance!!.create(Webservice::class.java)
            val call: Call<SearchResponse> = service.getVenues(coordinates, limit, radius, clientId, clientSecret, Util.getCurrentDate())

            call.enqueue(object : Callback<SearchResponse?> {
                override fun onResponse(call: Call<SearchResponse?>, response: Response<SearchResponse?>) {
                    val searchResponse: SearchResponse? = response.body()
                    searchResponse?.response?.venues?.let { venues ->
                        searchResult.value = venues
                        insert(venues, query, coordinates)
                    }
                }

                override fun onFailure(call: Call<SearchResponse?>, t: Throwable) {
                    TODO("Not yet implemented")
                }
            })
        } else {
            getVenue(coordinates)
        }
    }

    fun getVenueDetails(venueId: String) {
        if (Util.isOnline(getApplication())) {
            val service: Webservice = retrofitInstance!!.create(Webservice::class.java)
            val call: Call<VenueDetails> = service.getVenueDetails(venueId)

            call.enqueue(object : Callback<VenueDetails?> {
                override fun onResponse(call: Call<VenueDetails?>, response: Response<VenueDetails?>) {
                    val venueDetails: VenueDetails? = response.body()
                    venueDetails?.let { venues ->
                        venue.value = venues
                        insertDetails(venueDetails)
                    }
                }

                override fun onFailure(call: Call<VenueDetails?>, t: Throwable) {
                    TODO("Not yet implemented")
                }
            })
        } else {
            getVenueDetailsFromDB(venueId)
        }
    }
}