package com.luciansimo.venues.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.luciansimo.venues.R
import com.luciansimo.venues.adapter.VenueDetailsAdapter
import com.luciansimo.venues.databinding.FragmentVenueDetailsBinding
import com.luciansimo.venues.viewmodel.VenueViewModel

class VenueDetailsFragment : Fragment() {
    private val viewModel: VenueViewModel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return FragmentVenueDetailsBinding.inflate(inflater, container, false).apply {
            val linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            val venueDetailsAdapter = VenueDetailsAdapter()

            viewModel.venue.observe(viewLifecycleOwner, Observer { venue ->
                venueDetailsAdapter.setVenue(venue)
            })

            venueDetailsRecyclerView.apply {
                layoutManager = linearLayoutManager
                adapter = venueDetailsAdapter
            }

        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val venueId: String = arguments?.getString(getString(R.string.venue_id_key)).toString()
        viewModel.getVenueDetails(venueId)
    }
}