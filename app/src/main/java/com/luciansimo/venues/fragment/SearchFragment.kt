package com.luciansimo.venues.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.luciansimo.venues.R
import com.luciansimo.venues.adapter.SearchAdapter
import com.luciansimo.venues.databinding.FragmentSearchBinding
import com.luciansimo.venues.util.Util
import com.luciansimo.venues.viewmodel.VenueViewModel

class SearchFragment : Fragment() {
    private val limit: Int = 10
    private val radius: Int = 1000
    private val viewModel: VenueViewModel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return FragmentSearchBinding.inflate(inflater, container, false).apply {
            val linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            val searchAdapter = SearchAdapter{ venueId ->
                val bundle: Bundle = bundleOf(getString(R.string.venue_id_key) to venueId)
                findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment, bundle)
            }
            viewModel.searchResult.observe(viewLifecycleOwner, Observer { response ->
                if (response != null) {
                    searchAdapter.setData(response)
                }
            })

            venuesRecyclerView.apply {
                layoutManager = linearLayoutManager
                adapter = searchAdapter
            }

            searchBar.imeOptions = EditorInfo.IME_ACTION_DONE
            searchBar.setOnQueryTextListener(object :
                SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    if (Util.isOnline(requireContext())) handleOnlineMode(query) else handleOfflineMode(query)
                    return false
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    return false
                }
            })
        }.root
    }

    private fun handleOfflineMode(query: String) {
        viewModel.coordinates.observe(viewLifecycleOwner, Observer { coordinate ->
            if (coordinate != null) {
                viewModel.searchVenues(query, coordinate, limit, radius)
            } else {
                showSnackBar(getString(R.string.no_internet_error))
            }
        })
        viewModel.getCoordinate(query)
    }

    private fun handleOnlineMode(query: String) {
        val coordinates: String? = Util.getLocation(requireContext(), query)
        if (coordinates == null) {
            showSnackBar(getString(R.string.location_not_found_error))
        } else {
            viewModel.searchVenues(query, coordinates, limit, radius)
        }
    }

    private fun showSnackBar(message: String) {
        Snackbar.make(requireActivity().findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show()
    }
}